	.equ	img_mNum, 0
	.equ	img_rows, 12
	.equ	img_cols, 16
	.equ	img_arry, 20
	
	.equ	rgb_gArr, 24
	.equ	rgb_bArr, 28

	.data
errMsg:	.asciz	"Error graying unknown image type\n"
	.align 	2

	.text
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ getMinMax - This will output a contrasted image
@ in -> r0 = A pointer to the image array
@ in -> r1 = rows (i)
@ in -> r2 = cols (j)
@ out -> r0 = min
@ out -> r1 = max
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
getMinMax:
	stmfd	sp!, {r4-r8}

	mov	r7, #255		@@r7 will hold our min
	mov	r8, #0			@@r8 will hold our max

	mov	r3, #0			@@r3 will track i
	mov	r4, #0			@@r4 will track j
	ldr	r5, [r0]		@@get the pointer to the first array

getMinMaxloop:
	ldrb	r6, [r5, r4]		@@r6 will contain the actual value we need

	cmp	r6, r7			@@Set the min to r6 if it's less than our min
	movlt	r7, r6

	cmp	r6, r8			@@set the max if r6 is greater than our max
	movgt	r8, r6

	add	r4, r4, #1		@@increment j
	cmp	r4, r2			@@compare j to col
	blt	getMinMaxloop

	add	r3, r3, #1		@@increment i
	cmp	r3, r1			@@compare i to rows
	movlt	r4, #0			@@reset j
	ldrlt	r5, [r0, r3, lsl #2]	@@load pointer to next array
	blt	getMinMaxloop

	mov	r0, r7
	mov	r1, r8
	
	ldmfd	sp!, {r4-r8}
	mov	pc, lr


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ grayPixel - applies the gray function to the specified pixel
@ in -> r0 = image pointer
@ in -> r1 = rows (i)
@ in -> r2 = cols (j)
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
grayPixel:
	stmfd	sp!, {r4-r9}
	
	mov	r8, #0			@@r8 will track i
	mov	r9, #0			@@r9 will track j

grayPixelLoopi:
	ldr	r4, [r0, #img_arry]	@@get the red image array
	ldr	r4, [r4, r8, lsl #2]	@@get the red pixel row

	ldr	r5, [r0, #rgb_gArr]	@@get the green image array
	ldr	r5, [r5, r8, lsl #2]	@@get the green pixel row

	ldr	r6, [r0, #rgb_bArr]	@@get the blue image array
	ldr	r6, [r6, r8, lsl #2]	@@get the blue pixel row


grayPixelLoopj:

	ldrb	r7, [r4, r9]		@@get the red pixel value
	@@@@@@@@@@@@@@@@@@@@@ multiply r5 by 54 with shift adds @@@@@@@@@@@@@@@@@@@@@
	lsl	r3, r7, #1
	add	r3, r3, r7, lsl #2
	add	r3, r3, r7, lsl #4
	add	r3, r3, r7, lsl #5

	ldrb	r7, [r5, r9]		@@get the green pixel value
	@@@@@@@@@@@@@@@@@@@@@ multiply r5 by 184 with shift adds @@@@@@@@@@@@@@@@@@@@@
	add	r3, r3, r7, lsl #3
	add	r3, r3, r7, lsl #4
	add	r3, r3, r7, lsl #5
	add	r3, r3, r7, lsl #7

	ldrb	r7, [r6, r9]		@@get the blue pixel value
	@@@@@@@@@@@@@@@@@@@@@ multiply r5 by 18 with shift adds @@@@@@@@@@@@@@@@@@@@@
	add	r3, r3, r7, lsl #1
	add	r3, r3, r7, lsl #4

	add	r3, r3, #128		@@add 128 to r3
	lsr	r3, r3, #8		@@devide by 256
	cmp	r3, #255		@@if r3>255, r3=255
	movgt	r3, #255
	cmp	r3, #0			@@if r3<0, r3=0
	movlt	r3, #0

	strb	r3, [r4, r9]		@@store r3 in the array

	add	r9, r9, #1		@@increment j
	cmp	r9, r2			@@compare j to col
	blt	grayPixelLoopj

	add	r8, r8, #1		@@increment i
	cmp	r8, r1			@@compare i to rows
	movlt	r9, #0			@@reset j
	blt	grayPixelLoopi

	ldmfd	sp!, {r4-r9}
	mov	pc, lr			@@return from the function

	
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ grayscaleImage - turns an image grayscale
@ in -> r0 = netImage pointer
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	.global grayscaleImage
grayscaleImage:	
	stmfd	sp!, {r4, lr}
	mov	r4, r0			@@store the pointer to the image in r4
	ldrb	r1, [r0, #img_mNum]	@@Get the magic number

	cmp	r1, #51			@@3 = 51 in ascii
	cmpne	r1, #54			@@6 = 54 in ascii
	beq	grayRGBImage		@@process an rbg image if 3 or 6
	
	cmp	r1, #50			@@2 = 50 in ascii
	cmpne	r1, #52			@@4 = 52 in ascii
	beq	grayGRAYImage		@@process grey image if 2 or 4

	ldr	r0, =errMsg		@@print error message if nothing matches
	bl	printf
	mov	r0, #0
	bl	exit

grayRGBImage:
	sub	r1, r1, #1		@@decrement the magic number
	strb	r1, [r0, #img_mNum]	@@store the magic number 
	ldr	r1, [r0, #img_rows]	@@get the image rows
	ldr	r2, [r0, #img_cols]	@@get the image cols
	bl	grayPixel
	
	mov	r3, #img_arry
	add	r3, r3, #4
	ldr	r0, [r4, r3]		@@get the green image array
	ldr	r1, [r4, #img_rows]	@@get the image rows
	bl	free_pixel_array

	mov	r3, #img_arry
	add	r3, r3, #8
	ldr	r0, [r4, r3]		@@get the green image array
	ldr	r1, [r4, #img_rows]	@@get the image rows
	bl	free_pixel_array

grayGRAYImage:
	ldr	r0, [r4, #img_arry]	@@get the image array
	ldr	r1, [r4, #img_rows]	@@get the image rows
	ldr	r2, [r4, #img_cols]	@@get the image cols
	bl	getMinMax		@@r0=min, r1=max

	orr	r0, r0, r1, lsl #8	@@create the minmax structure

exitGray:
	ldmfd	sp!, {r4, lr}
	mov	pc,lr
